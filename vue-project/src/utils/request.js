import config from "@/config";
import axios from "axios";
import { getLocal } from "@/utils/local";
// import qs from "qs";

class HttpRequest {
  constructor() {
    this.timeout = config.timeout || 3000;
    this.baseURL = config.baseURL;
  }
  setInterceptors(instance) {
    instance.interceptors.request.use((config) => {
      config.headers.authorization = "Bearer" + getLocal("token");
      return config;
    });

    instance.interceptors.response.use(
      (res) => {
        if (res.status === 200) {
          return Promise.resolve(res.data);
        } else {
          return Promise.reject(res.data);
        }
      },
      (err) => {
        switch (err.response.status) {
          case "401": //无权限，跳回登录页
            console.log("无权限，跳回登录页");
            break;
          default:
            break;
        }
        return Promise.reject(err);
      }
    );
  }
  mergeOptions(options) {
    return {
      baseURL: this.baseURL,
      timeout: this.timeout,
      ...options,
    };
  }
  request(options) {
    let instance = axios.create();
    this.setInterceptors(instance);
    const opts = this.mergeOptions(options);
    return instance(opts);
  }
  get(url, config) {
    return this.request({
      method: "get",
      url,
      ...config,
    });
  }
  post(url, data) {
    return this.request({
      method: "post",
      url,
      data,
    });
  }
}

export default new HttpRequest();
