import { getLocal } from "@/utils/local";
import store from "../store";
import * as types from "@/store/action-type";

class WS {
  constructor(config = {}) {
    this.url = config.url || "vue.zhufengpeixun.cn";
    this.port = config.port || 80;
    this.protocol = config.protocol || "ws";
    this.time = config.time || 30000;
    this.ws = null;
  }
  onOpen = () => {
    this.ws.send(
      JSON.stringify({
        type: "auth",
        data: getLocal("token"),
      })
    );
  };
  onMessage = (e) => {
    let { type, data } = JSON.parse(e.data);
    switch (type) {
      case "noAuth":
        console.log("noAuth");
        break;
      case "heartCheck":
        this.checkServer();
        this.ws.send(JSON.stringify({ type: "heartCheck" }));
        break;
      default:
        store.commit(types.SET_MESSAGE, data);
    }
  };
  onError = () => {
    setTimeout(() => {
      this.create();
    }, 1000);
  };
  onClose = () => {
    this.ws.close();
  };
  create() {
    this.ws = new WebSocket(`${this.protocol}://${this.url}:${this.port}`);
    this.ws.onopen = this.onOpen;
    this.ws.onclose = this.onClose;
    this.ws.onmessage = this.onMessage;
    this.ws.onerror = this.onError;
  }
  checkServer() {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.onClose();
      this.onClose();
    }, this.time + 1000);
  }
  send = (msg) => {
    this.ws.send(JSON.stringify(msg));
  };
}

export default WS;
