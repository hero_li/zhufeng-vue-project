import Vue from "vue";
import VueRouter from "vue-router";
import hooks from "./hooks";
Vue.use(VueRouter);

let routes = [];

let fileContext = require.context("./", false, /\.router\.js$/);

fileContext.keys().forEach((key) => {
  routes.push(...fileContext(key).default);
});

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

Object.values(hooks).forEach((hook) => {
  router.beforeEach(hook.bind(router));
});

export default router;
