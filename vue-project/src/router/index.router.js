export default [
  {
    path: "/",
    name: "home",
    component: () => import("@/views/Home.vue"),
  },
  {
    path: "*",
    component: () => import("@/views/404.vue"),
  },
  {
    path: "/manager",
    component: () => import("@/views/manager/index.vue"),
    meta: {
      needLogin: true,
    },
  },
];
