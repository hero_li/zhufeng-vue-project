export default [
  {
    path: "/reg",
    name: "reg",
    component: () => import("@/views/user/Reg.vue"),
  },
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/user/Login.vue"),
  },
  {
    path: "/forget",
    name: "forget",
    component: () => import("@/views/user/Forget.vue"),
  },
];
