# 2.2vuex的配置

在store/index.js中如果代码内容太多的话，代码很拥挤，并且让人看起来很头疼。所以把store文件夹里代码模块化，更清晰找到对应的文件和使用方法。<br />store/index.js文件
```javascript
import Vue from 'vue'
import Vuex from 'vuex'
import rootModule from './rootModule';//在store里新建rootModule.js文件   根模块

Vue.use(Vuex);
//require.context()动态加载模块，实现store的状态分割
//读取modules的目录,不读子目录,匹配.js文件的正则表达式
const files = require.context('./modules', false, /\.js$/);
//console.log(files(key).default)//获得一个文件
files.keys().forEach(key => {
    // 模块对应的内容
    let store = files(key).default;
    let moduleName = key.replace(/^\.\//, '').replace(/\.js$/, '');
    // 动态的添加模块
    let modules = rootModule.modules = (rootModule.modules || {});
    modules[moduleName] = store;
    modules[moduleName].namespaced = true;
});

let store = new Vuex.Store(rootModule);
//console.log(store)
export default store;
```
store/rootModule.js文件
```javascript
const rootModule = {
    state: {},
    mutations: {},
    actions: {},
    modules: {}
};
export default rootModule;
```
既然有根模块就会有子模块，所以在store文件夹里新建modules文件夹，在里面新建user.js用户模块，article.js文章模块<br />store/modules文件
```javascript
//user.js
export default {
    state:{
        user:'用户'
    }
}
//article.js
export default {
    state:{
        article:'文章'
    }
}
```